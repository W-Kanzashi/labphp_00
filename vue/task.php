<?php
session_name("myid");
session_start(); 
var_dump($_SESSION);
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="shortcut icon" type="image/png" href="https://img.icons8.com/cotton/2x/checkmark.png">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="css/animation.css">
  <title>Add Task</title>
</head>
<body>
  <section class="d-flex w-50 mx-auto mt-5">
    <form method="POST" action="../controleur/FrontControleur.php?action=add_task">
      <div class="form-group">
        <label for="task">Task</label>
        <input type="text" class="form-control" id="task" name="task" aria-describedby="taskHelp" placeholder="Enter task">
      </div>
      <div class="form-group">
        <label for="note">Note</label>
        <input type="text" class="form-control" id="note" name="note" placeholder="Need to buy groceries">
      </div>
      <div class="form-group">
        <label for="dateEnd">Date de fin</label>
        <input type="date" name="dateEnd" id="dateEnd">
      </div>
      <button type="submit" class="btn btn-primary">Submit</button>
    </form>
  </section>
</body>
</html>
