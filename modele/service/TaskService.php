<?php 
namespace modele\service;

// Inclure le fichier des constantes : il contient notamment la constante LOGFILE
// include(__DIR__.'/../../Constantes.php');

class TaskService { 

    // Référence sur l'objet UtilisateurDao
    private $hTaskDao;

    /** 
    * Cette méthode un peu spéciale est le constructeur
    * Elle est exécutée lorsque vous créez un objet UtilisateurService
    */ 
    public function __construct() 
    {   
      // Enregistrement du message dans le fichier log
      error_log("UtilisateurService -> __construct()".PHP_EOL, 3, LOGFILE);

      try {
          // Instancier la classe UtilisateurDao : appel du constructeur __construct() 
          // Si problème, la classe UtilisateurDao lève une exception
          $this->hTaskDao = new \modele\dao\TaskDao();
      }
      // Propagation de l'exception : l'exception est transmise à la méthode appelante
      catch (\Exception $e) {
        throw new \Exception('Impossible d\'établir la connexion à la BD.');
      }
    } 
	
    /**  
    * Destructeur, appelé quand l'objet est détruit
    */  
    public function __destruct()  
    {  
      // Enregistrement du message dans le fichier log
      // error_log("TaskService -> __destruct".PHP_EOL, 3, LOGFILE);	
    }

    // Task
    public function createTask($task)
    { 
        // Enregistrement du message dans le fichier log
        error_log("TaskService -> createTask()".PHP_EOL, 3, LOGFILE);

        try {
          // Appel de la méthode create() de la classe UtilisateurDao
          // Retourne true si utilisateur créé SINON false
          $bRet = $this->hTaskDao->createTask($task);
        }
        // Propagation de l'exception : utilisateur existe déjà
        catch (\Exception $e) {
          throw new \Exception($e->getMessage());
        }

        // Retourne true si utilisateur créé SINON false 
        return $bRet;
    }

    public function deleteTask($id)
    { 
      // Enregistrement du message dans le fichier log
      error_log("TaskService -> deleteTask()".PHP_EOL, 3, LOGFILE);

      try {
          // Appel de la méthode deleteUser() de la classe UtilisateurDao
          // Retourne true si utilisateur créé SINON false
          $bRet = $this->hTaskDao->deleteTask($id);
      }
      // Propagation de l'exception : suppression impossible
      catch (\Exception $e) {
          throw new \Exception($e->getMessage());
      }

      // Retourne true si utilisateur a été supprimé 
      return $bRet;
    }
}
?>