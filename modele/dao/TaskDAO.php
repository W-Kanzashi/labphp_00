<?php 
namespace modele\dao;

// Inclure le fichier des constantes : il contient notamment la constante LOGFILE
//include(__DIR__.'/../../Constantes.php');

// Classe de gestion des accès à la base de données pour la table T_Task
class TaskDAO { 

    // Table des Tasks
    private const TABLE = "T_TASK";

    // Connexion à la base de données
    private $Connection;

    /** 
    * Cette méthode un peu spéciale est le constructeur
    * Elle est exécutée lorsque vous créez un objet TaskDAO
    */ 
    public function __construct() 
    { 
        // Enregistrement du message dans le fichier log
        error_log("TaskDAO -> __construct()".PHP_EOL, 3, LOGFILE);

        try {
            // Obtenir une connexion à la base
            // Mémorisation de la connexion dans l'attribut d'instance ($Connection) de la classe
            // Cette connexion est utilisée par les autres méthodes pour envoyer des requêtes
            $hconnection = new Connexion();
            $this->Connection = $hconnection->getConnection();
        }
        // Exception est levée si connexion à la BD impossible
        catch (\Exception $e) {
            // Créer une exception qui sera reçue par la méthode qui a effectué un new de cette classe
            throw new \Exception('Impossible d\'établir la connexion à la BD.');
        }
    }

    // Fonction de création d'un Task
    public function createTask($task) : bool
    { 
      // Enregistrement du message dans le fichier log
      error_log("TaskDAO -> create()".PHP_EOL, 3, LOGFILE);

      //
      // Création d'une nouvelle tache dans la base de données
      //

      $requete = $this->Connection->prepare("SELECT id FROM t_utilisateur WHERE prenom= :prenom limit 1");

      try {
        $requete->execute(array(
          "prenom" => strstr($_SESSION['prenom_nom'], ' ', true))
        );
        $user_id = $requete->fetch();
        var_dump($_SESSION);
        echo strstr($_SESSION['prenom_nom'], ' ', true);
        var_dump($user_id['id']);
      } catch (\Exception $e) {
        echo $e->getMessage();
      }

      // nom, prenom , email, motdepasse
      // Création d'une requête préparée
      $requete = $this->Connection->prepare("INSERT INTO ".self::TABLE." (task,note,date,user_id)
      VALUES (:task,:note,:date,:user_id)");

      // Exécution de la requête : execute() retourne si exécution requête ok sinon false
      $result = $requete->execute(array(
        "task" => $task->getTask(),
        "note" => $task->getNote(),
        "date" => $task->getDate(),
        "user_id" => $user_id['id']));

      // Fermer la connexion à la BDD
      $this->Connection = null;

      // Retourner le résultat
      return $result;
    }

    // Fonction de suppression d'un Task à partir de son id
    public function deleteTask($id)
    {
      // Enregistrement du message dans le fichier log
      error_log("TaskDAO -> deleteUser()".PHP_EOL, 3, LOGFILE);

      $bRet = true;
      try {
          // Création d'une requête préparée
          $requete = $this->Connection->prepare("DELETE FROM ".self::TABLE." WHERE id = ?");

          // Exécuter la requête
          $bRet = $requete->execute(array($id));
      }
      catch (\Exception $e) {
          $bRet = false;
          // Lever une exception : fin de la méthode
            throw new \Exception('Suppression compte impossible.');
      }

      // Fermer la connexion à la BDD
      $this->Connection = null; 

      // Retourne true si suppression ok sinon false
      return $bRet;
    }
    
    /**  
    * Destructeur, appelé quand l'objet est détruit
    */  
    public function __destruct()  
    {  
        // Enregistrement du message dans le fichier log
        // error_log("TaskDAO -> __destruct()".PHP_EOL, 3, LOGFILE);
    }
}

?>