<?php 
namespace modele\metier;

/* Visibilité des propriétés et méthodes
    public : 		n'importe qui a accès à la méthode ou à l'attribut demandé.
	protected : 	seule la classe ainsi que ses sous classes éventuelles 
                    (classes héritées).
	private : 		seule la classe ayant défini l'élément peut y accéder.
*/
class Task { 
	
    // Constantes de la classe Utilisateur
    private const CR = "\n";

    // Attributs d'instance de la classe Utilisateur
    // private : pas d'accès à ces attributs en dehors de la classe
    
    // User task
    private $id;
    private $task;
    private $note;
    private $date;

    /** 
    * Cette méthode un peu spéciale est le constructeur
    * Elle est exécutée lorsque vous créez un objet Utilisateur. 
    */ 
    public function __construct() 
    { 
    }

    // Méthode setter de l'attribut $id
    public function setId($id) : void {
        $this->id = $id;
    }

    // Méthode getter de l'attribut $id
    public function getId() {
        return $this->id;
    }

    public function setTask($task) : void {
        $this->task = $task;
    }

    public function getTask() {
        return $this->task;
    }

    public function setNote($note) : void {
        $this->note = $note;
    }

    public function getNote() {
        return $this->note;
    }

    public function setDate($date) : void {
        $this->date = $date;
    }

    public function getDate() {
        return $this->date;
    }
	
    /**  
    * Destructeur, appelé quand l'objet est détruit
    */  
    public function __destruct()  
    {  
    	// Libérer les ressources
        // unset() détruit la ou les variables dont le nom a été passé en argument
    	// unset($this->nom);
       	// unset($this->prenom);
       	// unset($this->mail);   	
    }
    
	public function __toString() 
	{    
	    return "id : $this->id nom : $this->nom  
        prenom : $this->prenom   
        mail : $this->mail".self::CR;   
	}
}
?>